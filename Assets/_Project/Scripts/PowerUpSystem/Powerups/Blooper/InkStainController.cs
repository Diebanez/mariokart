﻿using MLFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkStainController : SingletonMonoBehaviour<InkStainController>
{
    [SerializeField] private float Duration;
    private float m_Timer;
    public GameObject InkStainPage;

    // Update is called once per frame
    void Update()
    {
        if(m_Timer > 0)
        {
            m_Timer -= Time.deltaTime;
            InkStainPage.gameObject.SetActive(true);
        }
        else
        {
            InkStainPage.gameObject.SetActive(false);
        }
    }

    public void ActivateStain()
    {
        m_Timer = Duration;
    }
}
