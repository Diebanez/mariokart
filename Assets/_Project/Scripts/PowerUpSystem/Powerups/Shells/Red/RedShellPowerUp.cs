﻿using MarioKart.Track;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RedShellPowerUp : PowerUp
{
    public GameObject RedShellObjectAuto;
    public GameObject RedShellObjectManual;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        if (direction == PowerUpHandler.UseDirection.Behind)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(RedShellObjectManual, handler.BackSpawner.transform.position, Quaternion.LookRotation(handler.BackSpawner.transform.forward));
        }
        else if (direction == PowerUpHandler.UseDirection.Forward)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(RedShellObjectAuto, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
            obj.GetComponentInChildren<RedShellObject>().PowerUpOwner = handler;
        }
    }
}
