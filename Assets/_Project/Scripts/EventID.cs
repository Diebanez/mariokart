﻿namespace MLFramework.Events {
public enum EventID {
    #region Framework Events
    //Scene
    SceneLoaded,
    //Game Flow
    GameStateEnter,
    GameStateExit,
    #endregion

    #region Specific Events
    APressed,
    BPressed,
    RBPressed,
    LeftStickMoved,
    RaceStartCounterUpdate
    #endregion
}
}