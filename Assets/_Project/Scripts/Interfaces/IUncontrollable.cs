﻿namespace MarioKart.Interfaces
{
public interface IUncontrollable
{
    void BlockControls(float time);
}
}