﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Immunity : Effect
{
    public float ImmunityDuration;

    public override void ApplyEffect(GameObject go)
    {
        go.transform.parent.GetComponentInChildren<PowerUpHandler>().ApplyImmunity(ImmunityDuration);
    }
}
