﻿using MarioKart.Track;
using MLFramework.Core;
using TMPro;
using UnityEngine;

public class PlayerLapCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_LapText;

    private LapCounter m_Progressor;

    private void OnEnable()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_Progressor = GameObject.FindWithTag("Player").GetComponentInChildren<LapCounter>();
    }

    private void Update()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_LapText.text = Mathf.CeilToInt(m_Progressor.Lap + 1).ToString() + "/6";
    }
}