﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MarioKart.Track
{
    public class TrackMover : MonoBehaviour
    {
        private bool m_UseFixedUpdate = false;
        [SerializeField] private float m_Speed = 5.0f;
        [SerializeField, Range(0, 1)] private float m_StartOffset;
        [SerializeField] private bool m_MainTrack = false;

        [SerializeField] private bool m_FollowMode = false;
        private Vector3 m_FollowPosition;
        [HideInInspector] public Vector3 FollowPosition => m_FollowPosition;
        
        private float m_Progression = 0f;
        private float m_Step;
        private int m_SelectedTrack;
        
        [SerializeField] private bool m_IsOffset = false;
        [SerializeField] private float m_HeightOffset;


        private void Start()
        {
            m_Progression = m_StartOffset;
            if (m_MainTrack)
                m_SelectedTrack = 0;
            else
                m_SelectedTrack = Random.Range(0, TrackController.Tracks.Count);
            m_Step = m_Speed / TrackController.Tracks[m_SelectedTrack].RuntimeSpacing /
                     TrackController.Tracks[m_SelectedTrack].EvaluatedPoints.Length;
        }

        private void Update()
        {
            if(!m_UseFixedUpdate)
            {
                var previousPos = m_Progression;
                m_Progression += m_Step * Time.deltaTime;

                if (m_Speed > 0)
                {
                    while (m_Progression > 1f)
                    {
                        m_Progression -= 1f;
                    }
                }
                else
                {
                    while (m_Progression < 0f)
                    {
                        m_Progression += 1f;
                    }
                }

                if (!m_FollowMode)
                {
                    if (!m_IsOffset)
                    {
                        transform.position = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                    }
                    else
                    {
                        transform.position = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                        transform.position = new Vector3(transform.position.x, transform.position.y + m_HeightOffset, transform.position.z);
                    }
                }
                else
                {
                    if (!m_IsOffset)
                    {
                        m_FollowPosition = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                    }
                    else
                    {
                        m_FollowPosition = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                        m_FollowPosition = new Vector3(m_FollowPosition.x, m_FollowPosition.y + m_HeightOffset, m_FollowPosition.z);
                    }
                }


                var normal = (TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression) -
                              TrackController.Tracks[m_SelectedTrack].GetPosition(previousPos)).normalized;
                transform.localRotation = Quaternion.LookRotation(normal);
            }
        }

        private void FixedUpdate()
        {
            if(m_UseFixedUpdate)
            {
                var previousPos = m_Progression;
                m_Progression += m_Step * Time.deltaTime;

                if (m_Speed > 0)
                {
                    while (m_Progression > 1f)
                    {
                        m_Progression -= 1f;
                    }
                }
                else
                {
                    while (m_Progression < 0f)
                    {
                        m_Progression += 1f;
                    }
                }

                if (!m_FollowMode)
                {
                    if (!m_IsOffset)
                    {
                        transform.position = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                    }
                    else
                    {
                        transform.position = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                        transform.position = new Vector3(transform.position.x, transform.position.y + m_HeightOffset, transform.position.z);
                    }
                }
                else
                {
                    if (!m_IsOffset)
                    {
                        m_FollowPosition = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                    }
                    else
                    {
                        m_FollowPosition = TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression);
                        m_FollowPosition = new Vector3(m_FollowPosition.x, m_FollowPosition.y + m_HeightOffset, m_FollowPosition.z);
                    }
                }


                var normal = (TrackController.Tracks[m_SelectedTrack].GetPosition(m_Progression) -
                              TrackController.Tracks[m_SelectedTrack].GetPosition(previousPos)).normalized;
                transform.localRotation = Quaternion.LookRotation(normal);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, .5f);
        }

        public void SetupMover(float offset, bool isOffset, float height, float speed, bool isFollowMode, bool isMainTrack, bool useFixedUpdate)
        {
            m_StartOffset = offset;
            m_IsOffset = isOffset;
            m_HeightOffset = height;
            m_FollowMode = isFollowMode;
            m_Speed = speed;
            m_MainTrack = isMainTrack;
            m_UseFixedUpdate = useFixedUpdate;

            m_Progression = m_StartOffset;

            m_SelectedTrack = 0;
            m_Step = m_Speed / TrackController.Tracks[m_SelectedTrack].RuntimeSpacing /
                     TrackController.Tracks[m_SelectedTrack].EvaluatedPoints.Length;
        }
    }
}