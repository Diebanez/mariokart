﻿using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform m_Target;
    [SerializeField] private Transform m_LookTransform;
    [SerializeField] private float m_MinMovementSpeed = 10f;
    [SerializeField] private float m_MaxMovementSpeed = 50f;
    [SerializeField] private float m_MinDistance = 10;
    [SerializeField] private float m_MaxDistance = 20;

    [SerializeField] private float m_MinRotationSpeed = 45f;
    [SerializeField] private float m_MaxRotationSpeed = 270f;

    private float m_ActualMovementSpeed = 0;
    private float m_ActualDistance;

    private void FixedUpdate()
    {
        if (m_Target == null)
        {
            m_Target = GameObject.FindWithTag("PlayerCameraTarget").transform;
        }

        if (m_LookTransform == null)
        {
            m_LookTransform = GameObject.FindWithTag("PlayerCameraLookTarget").transform;
        }
        
        if (m_Target != null && m_LookTransform != null)
        {
            var distance = Vector3.Distance(transform.position, m_Target.position);
            distance = Mathf.Clamp(distance, m_MinDistance, m_MaxDistance);

            m_ActualDistance = distance;

            var lerpValue = 1 - Mathf.InverseLerp(m_MaxDistance, m_MinDistance, distance);
            var speed = Mathf.Lerp(m_MinMovementSpeed, m_MaxMovementSpeed, lerpValue);

            m_ActualMovementSpeed = speed;

            transform.position = Vector3.MoveTowards(transform.position, m_Target.position, speed * Time.fixedDeltaTime);

            var targetForward = (m_LookTransform.position - transform.position).normalized;
            var rotationAngle = Vector3.SignedAngle(transform.forward, targetForward, Vector3.up);

            var rotationLerpValue = Mathf.InverseLerp(0, 90, rotationAngle);
            var rotationSpeed = Mathf.Lerp(m_MinRotationSpeed, m_MaxRotationSpeed, rotationLerpValue);

            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                Quaternion.LookRotation(targetForward, Vector3.up), rotationSpeed * Time.fixedDeltaTime);
        }
    }
}