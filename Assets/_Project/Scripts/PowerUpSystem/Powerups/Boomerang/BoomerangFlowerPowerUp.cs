﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BoomerangFlowerPowerUp : PowerUp
{
	public GameObject BoomerangObject;

	public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
	{
		PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

		if (direction == PowerUpHandler.UseDirection.Forward)
		{
			base.UsePowerUp(go, direction);
			GameObject obj = Instantiate(BoomerangObject, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
            obj.GetComponentInChildren<BoomerangFlowerObject>().PowerUpOwner = handler;
            obj.GetComponentInChildren<BoomerangFlowerObject>().PowerUp = Instantiate(this);
		}
	}
}
