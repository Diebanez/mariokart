﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DropRateTable : ScriptableObject
{
    public DropQuota[] DropQuotas;
}
