﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MarioKart.Track
{
[RequireComponent(typeof(TrackProgressor))]
public class LapCounter : MonoBehaviour
{
    public UnityAction RaceEnd;
    
    [SerializeField] [Range(2, 8)] private int m_CheckPointCount = 4;

    private int m_Lap = 0;
    private TrackProgressor m_Progressor;
    private int m_ActualCheck = 0;
    private float[] m_CheckValues;

    public int Lap => m_Lap;

    public TrackProgressor Progressor => m_Progressor;

    private void Start()
    {
        m_Progressor = GetComponent<TrackProgressor>();
        m_CheckValues = new float[m_CheckPointCount];
        for (int i = 0; i < m_CheckPointCount; i++)
        {
            m_CheckValues[i] = (float) i / m_CheckPointCount;
        }
    }

    private void Update()
    {
        if (m_Progressor.ActualProgression > m_CheckValues[m_ActualCheck])
        {
            var nextCheck = m_ActualCheck + 1;
            if (nextCheck >= m_CheckPointCount)
            {
                nextCheck = 0;
                if (m_Progressor.ActualProgression < 1f)
                {
                    m_ActualCheck = nextCheck;
                    
                }
            }
            else
            {
                if (m_Progressor.ActualProgression < m_CheckValues[nextCheck])
                {
                    m_ActualCheck = nextCheck;
                    if (m_ActualCheck == 1)
                    {
                        m_Lap++;
                        if(m_Lap >= 6)
                            RaceEnd?.Invoke();
                    }
                }
            }
        }
    }
}
}