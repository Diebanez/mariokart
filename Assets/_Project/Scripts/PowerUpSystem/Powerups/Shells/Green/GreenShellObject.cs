﻿using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;

public class GreenShellObject : PowerUpObject
{
    public List<Effect> GreenShellEffects = new List<Effect>();

    [SerializeField] private int m_MaxBounces = 5;
    [SerializeField] private float m_MovementSpeed = 40f;
    [SerializeField] private float m_DistanceFromGround = 1f;

    private int m_BounceCounter = 0;
    private Rigidbody m_Rb;

    private void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
        m_BounceCounter = 0;
    }

    private void FixedUpdate()
    {
        var movement = transform.position + transform.forward * m_MovementSpeed * Time.fixedDeltaTime;
        RaycastHit hit;
        if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
        {
            var targetY = hit.point.y + m_DistanceFromGround;
            movement.y = targetY;
        }

        m_Rb.MovePosition(movement);
    }


    private void OnCollisionEnter(Collision otherCollision)
    {
        if (otherCollision.transform.parent != null && otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>() && !otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            var handler = otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>();
            if (handler)
            {
                if (!handler.IsImmune)
                {
                    if(handler.MainPowerUp != null)
                    {
                        if(handler.MainPowerUp.RotatorModel.Count > 0)
                        {
                            handler.UpdatePowerUpUsage();
                        }
                        else
                        {
                            foreach (var effect in GreenShellEffects)
                            {
                                effect.ApplyEffect(handler.m_Mover);
                            }
                            handler.DropCoins();
                        }
                    }
                    else
                    {
                        foreach (var effect in GreenShellEffects)
                        {
                            effect.ApplyEffect(handler.m_Mover);
                        }
                        handler.DropCoins();
                    }
                }
                Destroy(this.transform.gameObject);
            }
        }
        else
        {
            m_BounceCounter++;
            if (m_BounceCounter >= m_MaxBounces)
            {
                Destroy(this.transform.gameObject);
            }
            else
            {
                var pointNormal = otherCollision.contacts[0].normal.normalized;
                var collisionNormal = -transform.forward;

                var angle = Vector3.SignedAngle(pointNormal, collisionNormal, Vector3.up);
                
                var rotation = Quaternion.AngleAxis(-angle, Vector3.up);

                var newNormal = rotation * pointNormal;

                transform.forward = newNormal;
            }
        }
    }
}