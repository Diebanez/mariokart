﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SuperHornPowerUp : PowerUp
{
    public float SuperHornRadius;
    public Effect SuperHornEffect;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        Collider[] powerUpTargetsHit = Physics.OverlapSphere(handler.transform.position, SuperHornRadius);
        foreach(Collider powerup in powerUpTargetsHit)
        {
            if(powerup.GetComponentInChildren<PowerUpObject>())
            {
                Destroy(powerup.gameObject);
            }
        }

        Collider[] kartTargetsHit = Physics.OverlapSphere(handler.transform.position, SuperHornRadius);
        foreach(Collider kart in kartTargetsHit)
        {
            if(kart.GetComponentInChildren<PowerUpHandler>())
            {
                if (kart.GetComponentInChildren<PowerUpHandler>() != handler)
                {
                    if(!kart.GetComponentInChildren<PowerUpHandler>().IsImmune)
                    {
                        SuperHornEffect.ApplyEffect(kart.GetComponentInChildren<PowerUpHandler>().m_Mover);
                        kart.GetComponentInChildren<PowerUpHandler>().DropCoins();
                    }
                }
            }
        }
    }
}
