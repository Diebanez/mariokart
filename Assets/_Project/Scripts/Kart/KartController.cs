﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarioKart.Movement
{
public class KartController : MonoBehaviour
{
    public Transform KartModel;
    public Transform KartNormal;
    public Rigidbody Sphere;

    private float m_Speed;
    private float m_CurrentSpeed;
    private float m_Rotate;
    private float m_CurrentRotate;
    private int m_DriftDirection;
    private float m_DriftPower;
    private int m_DriftMode = 0;
    private bool m_FirstBoost;
    private bool m_SecondBoost;
    private bool m_ThirdBoost;

    [Header("Bools")]
    public bool Drifting;

    [Header("Parameters")]

    public float Acceleration = 30f;
    public float Steering = 80f;
    public float Gravity = 10f;
    public LayerMask LayerMask;

    void Update()
    {
        FollowCollider();
        Accelerate();
    }

    private void FixedUpdate()
    {
        //Forward Acceleration
        if (!Drifting)
            Sphere.AddForce(-KartModel.transform.right * m_CurrentSpeed, ForceMode.Acceleration);
        else
            Sphere.AddForce(transform.forward * m_CurrentSpeed, ForceMode.Acceleration);

        //Gravity
        Sphere.AddForce(Vector3.down * Gravity, ForceMode.Acceleration);

        //Steering
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(0, transform.eulerAngles.y + m_CurrentRotate, 0), Time.deltaTime * 5f);

        Physics.Raycast(transform.position + (transform.up * .1f), Vector3.down, out RaycastHit hitOn, 1.1f, LayerMask);
        Physics.Raycast(transform.position + (transform.up * .1f), Vector3.down, out RaycastHit hitNear, 2.0f, LayerMask);

        //Normal Rotation
        KartNormal.up = Vector3.Lerp(KartNormal.up, hitNear.normal, Time.deltaTime * 8.0f);
        KartNormal.Rotate(0, transform.eulerAngles.y, 0);
    }

    void FollowCollider()
    {
        transform.position = Sphere.transform.position - new Vector3(0, 0.4f, 0);
    }

    void Accelerate()
    {
        m_Speed = Acceleration;
    }

    void Boost()
    {
        Drifting = false;
        m_DriftPower = 0;
        m_DriftMode = 0;
        m_FirstBoost = false;
        m_SecondBoost = false;
        m_ThirdBoost = false;
    }

    void Steer(int direction, float amount)
    {
        m_Rotate = (Steering * direction) * amount;
    }

    public void NormalSteer(float horizontalInput)
    {
        int dir = horizontalInput > 0 ? 1 : -1;
        float amount = Mathf.Abs(horizontalInput);
        Steer(dir, amount);
    }

    public void Drift(float horizontalInput)
    {
        if (!Drifting && horizontalInput != 0)
        {
            Drifting = true;
            m_DriftDirection = horizontalInput > 0 ? 1 : -1;
        }

        if (Drifting)
        {
            float control = (m_DriftDirection == 1) ? Remap(horizontalInput, -1, 1, 0, 2) : Remap(horizontalInput, -1, 1, 2, 0);
            float powerControl = (m_DriftDirection == 1) ? Remap(horizontalInput, -1, 1, .2f, 1) : Remap(horizontalInput, -1, 1, 1, .2f);
            Steer(m_DriftDirection, control);
            m_DriftPower += powerControl;
        }

        m_CurrentSpeed = Mathf.SmoothStep(m_CurrentSpeed, m_Speed, Time.deltaTime * 12f);
        m_Speed = 0f;
        m_CurrentRotate = Mathf.Lerp(m_CurrentRotate, m_Rotate, Time.deltaTime * 4f);
        m_Rotate = 0f;
    }

    public void ExitDrift()
    {
        if (Drifting)
        {
            Boost();
        }
    }

    void SetKartModel(float horizontalInput)
    {
        if (!Drifting)
        {
            KartModel.localEulerAngles = Vector3.Lerp(KartModel.localEulerAngles, new Vector3(0, 90 + (horizontalInput * 15), KartModel.localEulerAngles.z), .2f);
        }
        else
        {
            float control = (m_DriftDirection == 1) ? Remap(horizontalInput, -1, 1, .5f, 2) : Remap(horizontalInput, -1, 1, 2, .5f);
            KartModel.parent.localRotation = Quaternion.Euler(0, Mathf.LerpAngle(KartModel.parent.localEulerAngles.y, (control * 15) * m_DriftDirection, .2f), 0);
        }
    }

    float Remap(float x, float x1, float x2, float y1, float y2)
    {
        float m = (y2 - y1) / (x2 - x1);
        float c = y1 - m * x1;
        return m * x + c;
    }
}
}
