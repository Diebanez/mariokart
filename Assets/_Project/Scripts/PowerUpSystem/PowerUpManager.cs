﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLFramework;

[CreateAssetMenu]
public class PowerUpManager : ScriptableObject
{
    #region Singleton Pattern

    private static PowerUpManager m_Instance;

    public static PowerUpManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = Resources.Load<PowerUpManager>("PowerUpManager");

                // If is called by editor and there's no PowerUpManager asset file, then create it
#if UNITY_EDITOR
                if (m_Instance == null)
                {
                    if (!System.IO.Directory.Exists(Application.dataPath + "/_Project"))
                        System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project");

                    if (!System.IO.Directory.Exists(Application.dataPath + "/_Project/Resources"))
                        System.IO.Directory.CreateDirectory(Application.dataPath + "/_Project/Resources");

                    UnityEditor.AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PowerUpManager>(),
                        "Assets/_Project/Resources/PowerUpManager.asset");
                    m_Instance = Resources.Load<PowerUpManager>("PowerUpManager");
                }
#endif
            }

            return m_Instance;
        }
    }

    #endregion


    public DropRateTable TableOfDropRates;

    public PowerUp GetRandomPowerUp(float distanceFromFirstPlayer)
    {
        DropQuota dropAllocation = new DropQuota();

        for (int i = 0; i < TableOfDropRates.DropQuotas.Length; i++)
        {
            if (distanceFromFirstPlayer < TableOfDropRates.DropQuotas[i].AllocatedDistance)
            {
                dropAllocation = TableOfDropRates.DropQuotas[i];
            }
        }

        List<PowerUp> drops = new List<PowerUp>();

        foreach (PowerUpDrop drop in dropAllocation.Drops)
        {
            for (int i = 0; i < drop.Chance; i++)
            {
                drops.Add(drop.PowerUp);
            }
        }

        int dice = UnityEngine.Random.Range(0, drops.Count - 1);
        return Instantiate(drops[dice]);
    }
}