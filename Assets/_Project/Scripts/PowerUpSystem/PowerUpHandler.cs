﻿using MarioKart.Movement;
using MLFramework.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpHandler : MonoBehaviour
{
    private PowerUp m_MainPowerUp;
    private PowerUp m_ReservePowerUp;
    private bool m_IsImmuneToOffensiveEffects = false;
    private bool m_IsGhosted = false;
    public bool IsImmune => m_IsImmuneToOffensiveEffects;
    private float m_ImmunityTimer;
    private float m_GhostTimer;
    private List<UseDirection> m_CurrentDirectionsAllowed = new List<UseDirection>();
    private bool m_PowerUpUsed = false;
    public bool PowerUpUsed => m_PowerUpUsed;
    private float m_PowerUpTimer = 0;
    private float m_PowerUpUsageTimer = 0;
    private int m_PowerUpUsage = 0;

    [HideInInspector] public int PowerUpUsage => m_PowerUpUsage;
    public PowerUp MainPowerUp => m_MainPowerUp;
    public PowerUp ReservePowerUp => m_ReservePowerUp;
    public GameObject m_Mover;
    public GameObject FrontSpawner;
    public GameObject RightSpawner;
    public GameObject BackSpawner;
    public GameObject LeftSpawner;
    public GameObject DroppableCoin;
    public PowerUpRotator[] Rotators;

    public enum UseDirection
    {
        Forward,
        Left,
        Right,
        Behind,
        None
    }
    public UseDirection m_DirectionUsage = UseDirection.None;

    private void Start()
    {
        m_ImmunityTimer = 0;
        m_IsImmuneToOffensiveEffects = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_PowerUpUsed)
        {
            if(m_PowerUpUsageTimer > 0)
            {
                m_PowerUpUsageTimer -= Time.deltaTime;
            }
            
            if(m_PowerUpTimer > 0)
            {
                m_PowerUpTimer -= Time.deltaTime;
            }
            else if(m_PowerUpTimer <= 0)
            {
                if(m_MainPowerUp != null)
                {
                    m_MainPowerUp.RemovePowerUp();
                    m_MainPowerUp = null;
                    UpdatePowerUpSlots();
                }
            }
        }

        if(m_IsImmuneToOffensiveEffects)
        {
            m_ImmunityTimer -= Time.deltaTime;
            if(m_ImmunityTimer <= 0)
            {
                m_IsImmuneToOffensiveEffects = false;
            }
        }

        if(m_IsGhosted)
        {
            if(m_GhostTimer > 0)
            {
                m_GhostTimer -= Time.deltaTime;
                this.gameObject.layer = LayerMask.NameToLayer("Ghost");
            }
            else
            {
                m_IsGhosted = false;
            }
        }
        else
        {
            this.gameObject.layer = LayerMask.NameToLayer("Kart");
        }
    }

    public void AssignPowerUp(PowerUp powerUp)
    {
        if(!m_MainPowerUp && !m_ReservePowerUp)
        {
            m_MainPowerUp = powerUp;
            m_PowerUpTimer = powerUp.Duration;
            m_PowerUpUsageTimer = 0;
            m_PowerUpUsage = powerUp.Amount;
            m_DirectionUsage = powerUp.DefaultDirection;
            m_CurrentDirectionsAllowed.Clear();
            foreach(UseDirection direction in powerUp.DirectionsAllowed)
            {
                m_CurrentDirectionsAllowed.Add(direction);
            }
            if(m_MainPowerUp.RotatorModel.Count > 0)
            {
                for(int i = 0; i < Rotators.Length; i++)
                {
                    Rotators[i].RemoveModel();
                }
                for(int j = 0; j < m_MainPowerUp.Amount; j++)
                {
                    Rotators[j].AddModel(m_MainPowerUp.RotatorModel[j]);
                }
            }
        }
        else if(m_MainPowerUp && !m_ReservePowerUp)
        {
            m_ReservePowerUp = powerUp;
        }
    }

    public void ApplyImmunity(float time)
    {
        m_IsImmuneToOffensiveEffects = true;
        m_ImmunityTimer = time;
    }

    public void ApplyGhost(float time)
    {
        m_IsGhosted = true;
        m_GhostTimer = time;
        m_IsImmuneToOffensiveEffects = true;
        m_ImmunityTimer = time;
    }

    public void DropCoins()
    {
        if(m_Mover.GetComponent<KartMover>().CollectedCoins > 3)
        {
            for(int i = 0; i < 3 && i < m_Mover.GetComponent<KartMover>().CollectedCoins ; i++)
            {
                m_Mover.GetComponent<KartMover>().RemoveCoin();
                GameObject go = Instantiate(DroppableCoin, new Vector3(BackSpawner.transform.position.x - 1, BackSpawner.transform.position.y, BackSpawner.transform.position.z + i), Quaternion.identity);
            }
        }
    }

    public void SetImmunity(bool value)
    {
        m_IsImmuneToOffensiveEffects = value;
    }

    public void UsePowerUp()
    {
        if(m_MainPowerUp)
        {
            if(!m_PowerUpUsed)
            {
                m_PowerUpUsed = true;

                if (m_MainPowerUp.Amount >= 1)
                {
                    if (m_PowerUpTimer > 0 && m_PowerUpUsageTimer <= 0)
                    {
                        m_MainPowerUp.UsePowerUp(m_Mover, m_DirectionUsage);
                        UpdatePowerUpUsage();

                        if(m_PowerUpUsage <= 0)
                        {
                            if (m_ReservePowerUp)
                            {
                                m_MainPowerUp.RemovePowerUp();
                                m_MainPowerUp = null;
                                UpdatePowerUpSlots();
                            }
                            else
                            {
                                m_MainPowerUp.RemovePowerUp();
                                m_MainPowerUp = null;
                                m_PowerUpUsed = false;
                                m_CurrentDirectionsAllowed.Clear();
                            }
                        }
                    }
                }
            }
            else
            {
                if (m_MainPowerUp.Amount >= 1)
                {
                    if (m_PowerUpTimer > 0 && m_PowerUpUsageTimer <= 0)
                    {
                        m_MainPowerUp.UsePowerUp(m_Mover, m_DirectionUsage);
                        UpdatePowerUpUsage();

                        if(m_PowerUpUsage <= 0)
                        {
                            if(m_ReservePowerUp)
                            {
                                m_MainPowerUp.RemovePowerUp();
                                m_MainPowerUp = null;
                                UpdatePowerUpSlots();
                            }
                            else
                            {
                                m_MainPowerUp.RemovePowerUp();
                                m_MainPowerUp = null;
                                m_PowerUpUsed = false;
                                m_CurrentDirectionsAllowed.Clear();
                            }
                        }
                    }
                }
            }
        }
    }


    private void UpdatePowerUpSlots()
    {
        if (!m_MainPowerUp && m_ReservePowerUp)
        {
            m_MainPowerUp = m_ReservePowerUp;
            m_PowerUpTimer = m_MainPowerUp.Duration;
            m_PowerUpUsageTimer = 0;
            m_PowerUpUsage = m_MainPowerUp.Amount;
            m_PowerUpUsed = false;
            if (m_MainPowerUp.RotatorModel.Count > 0)
            {
                for (int i = 0; i < Rotators.Length; i++)
                {
                    Rotators[i].RemoveModel();
                }
                for (int j = 0; j < m_MainPowerUp.Amount; j++)
                {
                    for(int k = 0; k < m_MainPowerUp.RotatorModel.Count; k++)
                    {
                        Rotators[j].AddModel(m_MainPowerUp.RotatorModel[k]);
                    }
                }
            }
            m_ReservePowerUp = null;
        }
    }

    public void UpdatePowerUpUsage()
    {
        m_PowerUpUsage--;
        if (m_MainPowerUp.RotatorModel.Count > 0)
        {
            Rotators[m_PowerUpUsage].RemoveModel();
        }

        m_PowerUpUsageTimer = m_MainPowerUp.CooldownUsage;
    }

    public bool CheckPowerUpSlotsAvailable()
    {
        if(m_MainPowerUp == null || m_ReservePowerUp == null)
        {
            return true;
        }
        else if(m_MainPowerUp != null && m_ReservePowerUp != null)
        {
            return false;
        }
        else
        {
            throw new System.Exception("To be true or not to be true, that's the question");
        }
    }

    public void RemoveCurrentPowerUp()
    {
        if(m_MainPowerUp != null)
        {
            if(m_PowerUpUsed)
            {
                while(m_PowerUpUsage > 0)
                {
                    UpdatePowerUpUsage();
                }

                if (m_PowerUpUsage <= 0)
                {
                    if (m_ReservePowerUp)
                    {
                        m_MainPowerUp.RemovePowerUp();
                        m_MainPowerUp = null;
                        UpdatePowerUpSlots();
                    }
                    else
                    {
                        m_MainPowerUp.RemovePowerUp();
                        m_MainPowerUp = null;
                        m_PowerUpUsed = false;
                        m_CurrentDirectionsAllowed.Clear();
                    }
                }
            }
            else
            {
                if (m_ReservePowerUp)
                {
                    m_MainPowerUp.RemovePowerUp();
                    m_MainPowerUp = null;
                    UpdatePowerUpSlots();
                }
                else
                {
                    m_MainPowerUp.RemovePowerUp();
                    m_MainPowerUp = null;
                    m_PowerUpUsed = false;
                    m_CurrentDirectionsAllowed.Clear();
                }
            }
        }
    }

    public PowerUp StealCurrentPowerUp()
    {
        PowerUp stolenPowerUp = new PowerUp();

        if (m_MainPowerUp != null)
        {
            stolenPowerUp = m_MainPowerUp;
            
            if (m_PowerUpUsed)
            {
                while (m_PowerUpUsage > 0)
                {
                    UpdatePowerUpUsage();
                }

                if (m_PowerUpUsage <= 0)
                {
                    if (m_ReservePowerUp)
                    {
                        m_MainPowerUp.RemovePowerUp();
                        m_MainPowerUp = null;
                        UpdatePowerUpSlots();
                    }
                    else
                    {
                        m_MainPowerUp.RemovePowerUp();
                        m_MainPowerUp = null;
                        m_PowerUpUsed = false;
                        m_CurrentDirectionsAllowed.Clear();
                    }
                }
            }
            else
            {
                if (m_ReservePowerUp)
                {
                    m_MainPowerUp.RemovePowerUp();
                    m_MainPowerUp = null;
                    UpdatePowerUpSlots();
                }
                else
                {
                    m_MainPowerUp.RemovePowerUp();
                    m_MainPowerUp = null;
                    m_PowerUpUsed = false;
                    m_CurrentDirectionsAllowed.Clear();
                }
            }

            return stolenPowerUp;
        }

        return null;
    }

    public void SetDirectionUsage(Vector2 axis)
    {
        if(m_CurrentDirectionsAllowed.Count > 0 && m_MainPowerUp)
        {
            if ((axis.x < 0.5f && axis.x > -0.5f) && axis.y >= 0.5f)
            {
                if (m_CurrentDirectionsAllowed.Contains(UseDirection.Forward))
                {
                    m_DirectionUsage = UseDirection.Forward;
                }
                else
                {
                    m_DirectionUsage = m_MainPowerUp.DefaultDirection;
                }
            }
            else if ((axis.x < 0.5f && axis.x > -0.5f) && axis.y <= -0.5f)
            {
                if (m_CurrentDirectionsAllowed.Contains(UseDirection.Behind))
                {
                    m_DirectionUsage = UseDirection.Behind;
                }
                else
                {
                    m_DirectionUsage = m_MainPowerUp.DefaultDirection;
                }
            }
            else if (axis.x >= 0.5f && (axis.y < 0.5f && axis.y > -0.5f))
            {
                if (m_CurrentDirectionsAllowed.Contains(UseDirection.Right))
                {
                    m_DirectionUsage = UseDirection.Right;
                }
                else
                {
                    m_DirectionUsage = m_MainPowerUp.DefaultDirection;
                }
            }
            else if (axis.x < -0.5f && (axis.y < 0.5f && axis.y > -0.5f))
            {
                if (m_CurrentDirectionsAllowed.Contains(UseDirection.Left))
                {
                    m_DirectionUsage = UseDirection.Left;
                }
                else
                {
                    m_DirectionUsage = m_MainPowerUp.DefaultDirection;
                }
            }
            else
            {
                m_DirectionUsage = m_MainPowerUp.DefaultDirection;
            }
        }
        else
        {
            m_DirectionUsage = UseDirection.None;
        }
    }
}
