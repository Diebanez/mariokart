﻿using UnityEditor;
using UnityEngine;

namespace MarioKart.Track.Editor
{
[CustomEditor(typeof(TrackController))]
public class TrackControllerEditor : UnityEditor.Editor
{
    private TrackController m_TargetTrack;

    private void OnEnable()
    {
        m_TargetTrack = (TrackController) target;
        if (m_TargetTrack.Track == null)
            m_TargetTrack.CleanTrack();
    }

    private void OnSceneGUI()
    {
        Input();
        Draw();
    }

    private void Input()
    {
        Event actualEvent = Event.current;
        if (actualEvent.type == EventType.KeyDown && actualEvent.isKey )
        {
            if (actualEvent.keyCode == KeyCode.A)
            {
                var lastSegmentPoints = m_TargetTrack.Track.GetPointsOfSegment(m_TargetTrack.Track.SegmentsCount - 1);
                var direction = (lastSegmentPoints[3] - lastSegmentPoints[0]).normalized;
                m_TargetTrack.Track.AddSegment(lastSegmentPoints[3] + direction * 5f);
                EditorUtility.SetDirty(m_TargetTrack);
            }else if (actualEvent.keyCode == KeyCode.C)
            {
                if(m_TargetTrack.Track.Closed)
                    m_TargetTrack.Track.Open();
                else
                    m_TargetTrack.Track.Close();
            }
        }
    }

    private void Draw()
    {
        if (m_TargetTrack == null) return;

        //Draw Bezier Curve and cap lines
        Handles.color = Color.white;
        for (int i = 0; i < m_TargetTrack.Track.SegmentsCount; i++)
        {
            Vector3[] points = m_TargetTrack.Track.GetPointsOfSegment(i);
            Handles.DrawBezier(points[0], points[3], points[1], points[2], Color.green, null, 2);
            Handles.DrawLine(points[0], points[1]);
            Handles.DrawLine(points[3], points[2]);
        }


        //Draw Movable Caps
        for (int i = 0; i < m_TargetTrack.Track.PointsCount; i++)
        {
            if (i % 3f == 0)
                Handles.color = Color.red;
            else
                Handles.color = Color.cyan;

            Vector3 newPosition = Handles.FreeMoveHandle(m_TargetTrack.Track.Points[i], Quaternion.identity, 1f, Vector3.zero, Handles.SphereHandleCap);

            if (m_TargetTrack.Track.Points[i] != newPosition)
            {
                /*
                RaycastHit hit;
                if (Physics.Raycast(Camera.current.ScreenPointToRay(Camera.current.WorldToScreenPoint(newPosition)), out hit, Mathf.Infinity, m_TargetTrack.SnapMask))
                {
                    newPosition = hit.point;
                }
*/
                Undo.RecordObject(m_TargetTrack, "Move Point");
                m_TargetTrack.Track.MovePoint(i, newPosition);
            }
        }
    }
}
}