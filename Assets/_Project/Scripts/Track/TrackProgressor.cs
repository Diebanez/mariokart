﻿using System;
using UnityEngine;

namespace MarioKart.Track
{
public class TrackProgressor : MonoBehaviour
{
    private float m_ActualProgression = 0f;

    public float ActualProgression => m_ActualProgression;

    private void Update()
    {
        float m_PreviousProgression = m_ActualProgression;
        
        var actualIndex = 0;
        var actualDistance = Mathf.Infinity;
        for (int i = 0; i < TrackController.MainTrack.EvaluatedPoints.Length; i++)
        {
            var newDistance = Vector3.Distance(transform.position, TrackController.MainTrack.EvaluatedPoints[i]);
            if (newDistance < actualDistance)
            {
                actualIndex = i;
                actualDistance = newDistance;
            }
        }

        m_ActualProgression = (float) actualIndex / (float) TrackController.MainTrack.EvaluatedPoints.Length;
    }
}
}