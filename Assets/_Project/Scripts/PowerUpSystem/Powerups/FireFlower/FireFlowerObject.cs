﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlowerObject : PowerUpObject
{
    public List<Effect> FireFlowerEffects = new List<Effect>();

    [SerializeField] private float m_MovementSpeed = 40f;
    [SerializeField] private float m_DistanceFromGround = 1f;
    [SerializeField] private float m_LifeTime = 3f;

    private Rigidbody m_Rb;
    public GameObject FireballModel;
    public float BounceFrequency;
    public float BounceAmplitude;

    private void Start()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if(m_LifeTime > 0)
        {
            var movement = transform.position + transform.forward * m_MovementSpeed * Time.fixedDeltaTime;
            RaycastHit hit;
            if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
            {
                var targetY = hit.point.y + m_DistanceFromGround;
                movement.y = targetY;
            }

            m_Rb.MovePosition(movement);
            m_LifeTime -= Time.fixedDeltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        FireballModel.transform.localPosition = new Vector3(0, Mathf.Sin(m_LifeTime * BounceFrequency) * BounceAmplitude, 0);
    }

    private void OnCollisionEnter(Collision otherCollision)
    {
        if(otherCollision.transform.parent != null && otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>() && !otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>().IsImmune)
        {
            var handler = otherCollision.transform.parent.GetComponentInChildren<PowerUpHandler>();
            if (handler)
            {
                if (!handler.IsImmune)
                {
                    if(handler.MainPowerUp != null)
                    {
                        if (handler.MainPowerUp.RotatorModel.Count > 0)
                        {
                            handler.UpdatePowerUpUsage();
                        }
                        else
                        {
                            foreach (var effect in FireFlowerEffects)
                            {
                                effect.ApplyEffect(handler.m_Mover);
                            }
                            handler.DropCoins();
                        }
                    }
                    else
                    {
                        foreach (var effect in FireFlowerEffects)
                        {
                            effect.ApplyEffect(handler.m_Mover);
                        }
                        handler.DropCoins();
                    }
                }
                Destroy(this.transform.gameObject);
            }
        }
        else
        {
            var pointNormal = otherCollision.contacts[0].normal.normalized;
            var collisionNormal = -transform.forward;

            var angle = Vector3.SignedAngle(pointNormal, collisionNormal, Vector3.up);

            var rotation = Quaternion.AngleAxis(-angle, Vector3.up);

            var newNormal = rotation * pointNormal;

            transform.forward = newNormal;
        }
    }
}
