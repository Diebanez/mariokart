﻿using System.Collections;
using MLFramework;
using UnityEngine;
using System.Collections.Generic;
using MarioKart.Movement;
using MLFramework.Events;

public class RaceManager : SingletonMonoBehaviour<RaceManager>
{
    [SerializeField] private List<Transform> m_SpawnPoints;
    [SerializeField] private PlayerController m_PlayerPrefab;
    [SerializeField] private PlayerController m_AIPrefab;
    [SerializeField] private List<KartStats> m_PossibleKarts;

    private List<PlayerController> m_SpawnedPlayers = new List<PlayerController>();
    
    public static void StartRace()
    {
        var playerPos = Random.Range(0, Instance.m_SpawnPoints.Count);

        for (int i = 0; i < Instance.m_SpawnPoints.Count; i++)
        {
            if (i != playerPos)
            {
                var newKart = Instantiate(Instance.m_AIPrefab, Instance.m_SpawnPoints[i].position, Instance.m_SpawnPoints[i].rotation);
                newKart.TargetKartStats = Instance.m_PossibleKarts[Random.Range(0, Instance.m_PossibleKarts.Count)];
                Instance.m_SpawnedPlayers.Add(newKart);
            }
            else
            {
                var newKart = Instantiate(Instance.m_PlayerPrefab, Instance.m_SpawnPoints[i].position, Instance.m_SpawnPoints[i].rotation);
                newKart.TargetKartStats = PlayerManager.SelectedStats;
                Instance.m_SpawnedPlayers.Add(newKart);
            }
        }
        Instance.StartCoroutine(Instance.RaceStartCount());
    }

    public static List<PlayerController> GetLeaderboard()
    {
        List<PlayerController> result = new List<PlayerController>();
        
        foreach (var spawnedPlayer in Instance.m_SpawnedPlayers)
        {
            var i = 0;
            while (i < result.Count)
            {
                if(result[i].Lap.Lap < spawnedPlayer.Lap.Lap || (result[i].Lap.Lap == spawnedPlayer.Lap.Lap && result[i].Lap.Progressor.ActualProgression < spawnedPlayer.Lap.Progressor.ActualProgression))
                    i++;
                else
                    break;
            }

            result.Insert(i, spawnedPlayer);
        }

        return result;
    }

    private IEnumerator RaceStartCount()
    {
        yield return new WaitForSeconds(3f);
        EventsManager.TriggerEvent(EventID.RaceStartCounterUpdate, null, 3);
        yield return new WaitForSeconds(1);
        EventsManager.TriggerEvent(EventID.RaceStartCounterUpdate, null, 2);
        yield return new WaitForSeconds(1);
        EventsManager.TriggerEvent(EventID.RaceStartCounterUpdate, null, 1);
        yield return new WaitForSeconds(1);
        EventsManager.TriggerEvent(EventID.RaceStartCounterUpdate, null, 0);
        foreach (var player in m_SpawnedPlayers)
        {
            player.GetComponentInChildren<KartMover>().Enabled = true;
        }
    }
}