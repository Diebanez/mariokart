﻿using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CoinPowerUp : PowerUp
{
    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        go.GetComponent<KartMover>().AddCoin();
    }
}
