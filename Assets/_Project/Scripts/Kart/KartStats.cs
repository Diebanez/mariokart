﻿using UnityEngine;

namespace MarioKart.Movement
{
[CreateAssetMenu]
public class KartStats : ScriptableObject
{
    [Header("View Settings")]
    [SerializeField] private Sprite m_KartSprite;
    [SerializeField] private GameObject m_KartPrefab;
    
    [Header("Control Settings")]
    [SerializeField] private float m_GroundSpeed;
    [SerializeField] private float m_GroundBrake;
    [SerializeField] private float m_GroundAcceleration;
    [SerializeField] private float m_GroundWeight;
    [SerializeField] private float m_GroundHandling;
    [SerializeField] private float m_GroundGrip;

    public Sprite KartSprite => m_KartSprite;
    public GameObject KartPrefab => m_KartPrefab;
    
    public float GroundSpeed => m_GroundSpeed;
    public float GroundBrake => m_GroundBrake;
    public float GroundAcceleration => m_GroundAcceleration;
    public float GroundWeight => m_GroundWeight;
    public float GroundHandling => m_GroundHandling;
    public float GroundGrip => m_GroundGrip;
}
}