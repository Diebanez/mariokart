﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SetEventSystemSelected : MonoBehaviour
{
    public void Trigger()
    {
        var eventSystem = GameObject.FindWithTag("EventSystem").GetComponent<EventSystem>();
        eventSystem.SetSelectedGameObject(gameObject);
    }
}
