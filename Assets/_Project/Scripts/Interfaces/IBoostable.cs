﻿namespace MarioKart.Interfaces
{
public interface IBoostable
{
    void AddBoost(float amount, float time);
}
}