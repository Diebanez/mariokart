﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using MarioKart.Movement;
using UnityEngine.EventSystems;

public class KartUIPool : MonoBehaviour
{
    [SerializeField] private List<KartStats> m_Karts;
    [SerializeField] private Button m_KartSelectButton;
    [SerializeField] private Transform m_KartParent;

    private void Start()
    {
        foreach (var kart in m_Karts)
        {
            var newButton = Instantiate(m_KartSelectButton, transform);
            newButton.image.sprite = kart.KartSprite;
            newButton.onClick.AddListener(() => { OnKartClicked(kart); });
            newButton.GetComponent<InputEventListener>().Selected.AddListener(() => { OnKartSelected(kart); });
        }

        if (transform.childCount > 0)
        {
            var eventSystem = GameObject.FindWithTag("EventSystem").GetComponent<EventSystem>();
            var firstObject = transform.GetChild(0);
            eventSystem.SetSelectedGameObject(firstObject.gameObject);
        }

        LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
    }

    private void OnEnable()
    {
        if (transform.childCount > 0)
        {
            var eventSystem = GameObject.FindWithTag("EventSystem").GetComponent<EventSystem>();
            var firstObject = transform.GetChild(0);
            eventSystem.SetSelectedGameObject(firstObject.gameObject);
        }
    }

    private void OnKartSelected(KartStats targetKart)
    {
        if (m_KartParent.transform.childCount > 0)
            DestroyImmediate(m_KartParent.GetChild(0).gameObject);
        Instantiate(targetKart.KartPrefab, m_KartParent);
    }

    private void OnKartClicked(KartStats targetKart)
    {
        PlayerManager.SelectKartAndStartGame(targetKart);
    }
}