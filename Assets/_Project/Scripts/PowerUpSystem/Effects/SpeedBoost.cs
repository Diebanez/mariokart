﻿using MarioKart.Interfaces;
using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpeedBoost : Effect
{
    [SerializeField]
    private float BoostValue;
    [SerializeField]
    private float BoostDuration;

    public override void ApplyEffect(GameObject go)
    {
        go.GetComponent<IBoostable>().AddBoost(BoostValue, BoostDuration);
    }
}
