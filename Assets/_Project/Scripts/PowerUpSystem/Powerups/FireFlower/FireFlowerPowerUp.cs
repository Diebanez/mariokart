﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FireFlowerPowerUp : PowerUp
{
    public GameObject FireFlowerObject;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        switch(direction)
        {
            case PowerUpHandler.UseDirection.Behind:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(FireFlowerObject, handler.BackSpawner.transform.position, Quaternion.LookRotation(handler.BackSpawner.transform.forward));
                    break;
                }
            case PowerUpHandler.UseDirection.Forward:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(FireFlowerObject, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
                    break;
                }
            case PowerUpHandler.UseDirection.Left:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(FireFlowerObject, handler.LeftSpawner.transform.position, Quaternion.LookRotation(handler.LeftSpawner.transform.forward));
                    break;
                }
            case PowerUpHandler.UseDirection.Right:
                {
                    base.UsePowerUp(go, direction);
                    GameObject obj = Instantiate(FireFlowerObject, handler.RightSpawner.transform.position, Quaternion.LookRotation(handler.RightSpawner.transform.forward));
                    break;
                }
        }
    }
}
