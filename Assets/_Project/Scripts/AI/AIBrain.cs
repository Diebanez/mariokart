﻿using System;
using MarioKart.Interfaces;
using MarioKart.Track;
using MLFramework.Prefabs;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MarioKart.AI
{
public class AIBrain : MonoBehaviour
{
    [SerializeField] [Prefab] private string m_FollowerPrefab;
    [SerializeField] private float m_MaxFollowerDistance = 5f;
    
    private IMovable m_Car;
    private TrackMover m_FollowTarget;
    [SerializeField] private PowerUpHandler m_PowerupHandler;

    private bool m_Drifting = false;
    private float m_Timer = 0f;


    public IMovable Car => m_Car ?? (m_Car = GetComponent<IMovable>());

    public TrackMover FollowTarget
    {
        get
        {
            if (m_FollowTarget == null)
            {
                m_FollowTarget = PrefabManager.InstantiatePrefab(m_FollowerPrefab).GetComponent<TrackMover>();
                m_FollowTarget.transform.position = transform.position;
            }

            return m_FollowTarget;
        }
    }

    private void Update()
    {
        m_Timer += Time.deltaTime;
        
        if (m_Timer >= 1f && m_PowerupHandler!= null && m_PowerupHandler.MainPowerUp != null)
        {
            if (Random.Range(0, 2) < 1)
            {
                m_PowerupHandler.SetDirectionUsage(new Vector2(0f, 0f));
                m_PowerupHandler.UsePowerUp();
            }
        }

        if (m_Timer >= 1f)
            m_Timer = 0f;
        
        if (Vector3.Distance(transform.position, FollowTarget.transform.position) > m_MaxFollowerDistance)
            FollowTarget.enabled = false;
        else
            FollowTarget.enabled = true;

        if (Vector3.Dot(transform.forward, (m_FollowTarget.transform.position - transform.position).normalized) > 0)
        {
            Car.Accelerate(true);
        }
        else
        {
            Car.Accelerate(false);
        }

        var rotation = 1 - Vector3.Dot(transform.forward, (m_FollowTarget.transform.position - transform.position).normalized);
        if (transform.InverseTransformPoint(m_FollowTarget.transform.position).x < 0)
            rotation = -rotation;
        if (Mathf.Abs(rotation) < .1f)
            rotation = 0;
        if (Mathf.Abs(rotation) > .25f)
            rotation += .35f * Mathf.Sign(rotation);
        
        Car.Steer(rotation);

        if (Mathf.Abs(rotation) > .3f)
        {
            if (!m_Drifting)
            {
                Car.Drift(true);
                m_Drifting = true;
            }
        }
        else if (Mathf.Abs(rotation) < .235f)
            if (m_Drifting)
            {
                Car.Drift(false);
                m_Drifting = false;
            }
    }
}
}