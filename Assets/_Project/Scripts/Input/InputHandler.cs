﻿using System;
using MarioKart.Interfaces;
using MLFramework.Events;
using UnityEngine;

namespace MarioKart.Input
{
    public class InputHandler : MonoBehaviour
    {
        private IMovable m_Mover;
        private PowerUpHandler m_PowerupHandler;

        public enum InputType
        {
            Console,
            Keyboard
        }

        public InputType TypeOfInput;

        private void Start()
        {
            m_Mover = GetComponent<IMovable>();
            m_PowerupHandler = transform.parent.GetComponentInChildren<PowerUpHandler>();
        }

        private void Update()
        {
            m_Mover.Steer(UnityEngine.Input.GetAxis("Horizontal"));
            switch (TypeOfInput)
            {
                case InputType.Console:
                {
                    if (UnityEngine.Input.GetKey(KeyCode.Joystick1Button0))
                        m_Mover.Accelerate(true);

                    if (UnityEngine.Input.GetKey(KeyCode.Joystick1Button1))
                        m_Mover.Brake(true);

                    if (UnityEngine.Input.GetKeyDown(KeyCode.Joystick1Button5))
                        m_Mover.Drift(true);
                    else if (UnityEngine.Input.GetKeyUp(KeyCode.Joystick1Button5))
                        m_Mover.Drift(false);

                    if (UnityEngine.Input.GetKeyDown(KeyCode.Joystick1Button4))
                    {
                        if (m_PowerupHandler.MainPowerUp != null)
                        {
                            Vector2 stickValue = Vector2.zero;
                            stickValue.x = UnityEngine.Input.GetAxis("Horizontal");
                            stickValue.y = UnityEngine.Input.GetAxis("Vertical");
                            m_PowerupHandler.SetDirectionUsage(stickValue);
                            m_PowerupHandler.UsePowerUp();
                        }
                    }

                    break;
                }
                case InputType.Keyboard:
                {
                    if (UnityEngine.Input.GetKey(KeyCode.W))
                        m_Mover.Accelerate(true);

                    if (UnityEngine.Input.GetKey(KeyCode.S))
                        m_Mover.Brake(true);

                    break;
                }
            }
            
            if(UnityEngine.Input.GetKeyDown(KeyCode.S))
                GetComponent<IStunnable>().Stun(1f);
            
            if(UnityEngine.Input.GetKeyDown(KeyCode.A))
                GetComponent<IUncontrollable>().BlockControls(1f);
        }
    }
}