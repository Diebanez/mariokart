﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Crazy8PowerUp : PowerUp
{
    [SerializeField] private List<PowerUp> m_PowerUps = new List<PowerUp>();
    private int m_PowerUpIndex = 0;
    private PowerUp m_CurrentPowerUp;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        m_PowerUpIndex = go.transform.parent.GetComponentInChildren<PowerUpHandler>().PowerUpUsage;
        m_CurrentPowerUp = Instantiate(m_PowerUps[m_PowerUpIndex -1]);
        m_CurrentPowerUp.UsePowerUp(go, direction);
    }
}
