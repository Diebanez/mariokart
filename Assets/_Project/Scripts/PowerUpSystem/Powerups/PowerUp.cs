﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PowerUp : ScriptableObject
{

    [SerializeField]
    public TypeOfTarget Target;

    #region VARIABLES
    public enum TypeOfTarget
    {
        Automatic,
        Manual
    }

    public PowerUpHandler.UseDirection DefaultDirection;
    public List<PowerUpHandler.UseDirection> DirectionsAllowed = new List<PowerUpHandler.UseDirection>();

    [SerializeField]
    public int Amount;
    [SerializeField]
    public float Duration;
    [SerializeField]
    public float CooldownUsage;
    public List<GameObject> RotatorModel = new List<GameObject>();

    [SerializeField] private Sprite m_PowerupSprite;

    public Sprite PowerupSprite => m_PowerupSprite;
    #endregion

    #region METHODS
    public virtual void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
    }

    public virtual void RemovePowerUp()
    {
    }
    #endregion
}