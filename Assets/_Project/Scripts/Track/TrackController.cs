﻿using System.Collections.Generic;
using MLFramework;
using UnityEditor;
using UnityEngine;

namespace MarioKart.Track
{
public class TrackController : MonoBehaviour
{
    public static TrackController MainTrack;
    public static List<TrackController> Tracks = new List<TrackController>();

    [SerializeField] private LayerMask m_SnapMask;
    [SerializeField] private float m_RuntimeSpacing = 1.0f;
    [SerializeField] private float m_RuntimeResolution = 1.0f;
    [SerializeField] private bool m_IsMainTrack = false;
    [SerializeField, HideInInspector] private TrackInfo m_Track;

    private Vector3[] m_EvaluatedPoints;

    public LayerMask SnapMask => m_SnapMask;
    public float RuntimeSpacing => m_RuntimeSpacing;
    public TrackInfo Track => m_Track;
    public Vector3[] EvaluatedPoints => m_EvaluatedPoints ?? (m_EvaluatedPoints = Track.EvaluatePoints(m_RuntimeSpacing, m_RuntimeResolution));

    private void Awake()
    {
        if (m_IsMainTrack)
            MainTrack = this;
        Tracks.Add(this);
    }

    public void CleanTrack()
    {
        m_Track = new TrackInfo(transform.position);
    }

    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (EditorApplication.isPlaying)
        {
            if (EvaluatedPoints != null)
            {
                foreach (var point in EvaluatedPoints)
                {
                    Gizmos.DrawSphere(point, .25f);
                }
            }
        }
    }
    
    #endif

    public Vector3 GetPosition(float time)
    {
        time = Mathf.Clamp01(time);
        time = Utils.Remap(time, 0, 1, 0, EvaluatedPoints.Length);
        var index = (int) (time - time % 1);
        var lerpTime = time % 1;
        if (index < EvaluatedPoints.Length - 1)
        {
            return Vector3.Lerp(m_EvaluatedPoints[index], m_EvaluatedPoints[index + 1], lerpTime);
        }

        return Vector3.Lerp(m_EvaluatedPoints[index], m_EvaluatedPoints[0], lerpTime);
    }
}
}