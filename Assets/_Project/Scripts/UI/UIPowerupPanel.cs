﻿using System;
using MLFramework.Core;
using UnityEngine;
using UnityEngine.UI;

public class UIPowerupPanel : MonoBehaviour
{
    [SerializeField] private Sprite m_DefaultSprite;
    [SerializeField] private Image m_MainPowerup;
    [SerializeField] private Image m_SecondaryPowerup;

    private PowerUpHandler m_TargetHandler;

    private void OnEnable()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_TargetHandler = GameObject.FindWithTag("Player").GetComponentInChildren<PowerUpHandler>();
    }

    private void Update()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
        {
            if (m_TargetHandler.MainPowerUp == null)
                m_MainPowerup.sprite = m_DefaultSprite;
            else
                m_MainPowerup.sprite = m_TargetHandler.MainPowerUp.PowerupSprite;

            if (m_TargetHandler.ReservePowerUp == null)
                m_SecondaryPowerup.sprite = m_DefaultSprite;
            else
                m_SecondaryPowerup.sprite = m_TargetHandler.ReservePowerUp.PowerupSprite;
        }
    }
}