﻿using MarioKart.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GoldernMushroomPowerUp : PowerUp
{
    [SerializeField]
    private Effect m_PowerUpEffect;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        base.UsePowerUp(go, direction);
        m_PowerUpEffect.ApplyEffect(go);
    }
}
