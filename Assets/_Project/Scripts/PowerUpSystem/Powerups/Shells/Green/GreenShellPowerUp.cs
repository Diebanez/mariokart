﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GreenShellPowerUp : PowerUp
{
    public GameObject GreenShellObject;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        if (direction == PowerUpHandler.UseDirection.Behind)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(GreenShellObject, handler.BackSpawner.transform.position, Quaternion.LookRotation(handler.BackSpawner.transform.forward));
        }
        else if (direction == PowerUpHandler.UseDirection.Forward)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(GreenShellObject, handler.FrontSpawner.transform.position, Quaternion.LookRotation(handler.FrontSpawner.transform.forward));
        }
    }
}
