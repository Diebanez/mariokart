﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangFlowerObject : PowerUpObject
{
	public List<Effect> BoomerangEffect = new List<Effect>();

	[SerializeField] private float m_MovementSpeed;
	[SerializeField] private float m_MaxDistance;
	[SerializeField] private float m_DistanceFromGround;
	[HideInInspector]
	public PowerUpHandler PowerUpOwner;
	[HideInInspector]
	public PowerUp PowerUp;
	private Vector3 m_StartPosition;
	private Rigidbody m_Rb;
    private enum TravelStatus
    {
        Forth,
        Backwards
    }
    private TravelStatus m_CurrentTravel = TravelStatus.Forth;

	// Start is called before the first frame update
    void Start()
    {
		m_Rb = GetComponent<Rigidbody>();
		m_StartPosition = transform.position;
        m_CurrentTravel = TravelStatus.Forth;
    }

	private void FixedUpdate()
	{
        if(m_CurrentTravel == TravelStatus.Forth)
        {
            if (Vector3.Distance(m_StartPosition, transform.position) < m_MaxDistance)
            {
                var movement = transform.position + transform.forward * m_MovementSpeed * Time.fixedDeltaTime;
                RaycastHit hit;
                if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
                {
                    var targetY = hit.point.y + m_DistanceFromGround;
                    movement.y = targetY;
                }

                m_Rb.MovePosition(movement);
            }
            else if (Vector3.Distance(m_StartPosition, transform.position) >= m_MaxDistance)
            {
                m_CurrentTravel = TravelStatus.Backwards;
            }
        }
        else if(m_CurrentTravel == TravelStatus.Backwards)
        {
            Debug.Log("Returning");
            var movement = transform.position + ((PowerUpOwner.transform.position - transform.position).normalized * m_MovementSpeed * Time.fixedDeltaTime);
            RaycastHit hit;
            if (Physics.Raycast(movement, Vector3.down, out hit, 10f))
            {
                var targetY = hit.point.y + m_DistanceFromGround;
                movement.y = targetY;
            }

            m_Rb.MovePosition(movement);
        }
	}

	private void OnTriggerEnter(Collider other)
	{
		var handler = other.transform.parent.GetComponentInChildren<PowerUpHandler>();
		if (handler)
		{
			if (!handler.IsImmune)
			{
				if(handler != PowerUpOwner)
				{
					foreach (var effect in BoomerangEffect)
					{
						effect.ApplyEffect(handler.m_Mover);
					}
                    handler.DropCoins();
                }
				else
				{
                    if(m_CurrentTravel == TravelStatus.Backwards)
                    {
                        if (PowerUp.Amount > 1)
                        {
                            PowerUp.Amount--;
                            handler.AssignPowerUp(PowerUp);
                            Destroy(this.gameObject);
                        }
                        else
                        {
                            Destroy(this.gameObject);
                        }
                    }
				}
			}
            else
            {
                if(handler != PowerUpOwner)
                {
                    Destroy(this.gameObject);
                }
                else
                {
                    if (m_CurrentTravel == TravelStatus.Backwards)
                    {
                        if (PowerUp.Amount > 1)
                        {
                            PowerUp.Amount--;
                            handler.AssignPowerUp(PowerUp);
                            Destroy(this.gameObject);
                        }
                        else
                        {
                            Destroy(this.gameObject);
                        }
                    }
                }
            }
		}
	}
}
