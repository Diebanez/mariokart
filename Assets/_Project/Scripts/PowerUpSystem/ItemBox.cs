﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    public GameObject ItemBoxGFX;
    private Collider ItemBoxCollider;

    [SerializeField]
    private float m_RespawnTime;
    private float m_RespawnTimer;

    private void Start()
    {
        ItemBoxCollider = GetComponentInChildren<Collider>();
        SetItemBox(true);
        m_RespawnTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_RespawnTimer >= 0)
        {
            SetItemBox(false);
            m_RespawnTimer -= Time.deltaTime;
        }
        else
        {
            SetItemBox(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (ItemBoxGFX.activeSelf)
        {
            if (other.GetComponent<PowerUpHandler>())
            {
                if(other.GetComponent<PowerUpHandler>().CheckPowerUpSlotsAvailable())
                {
                    other.GetComponent<PowerUpHandler>().AssignPowerUp(PowerUpManager.Instance.GetRandomPowerUp(0));
                    SetItemBox(false);
                    m_RespawnTimer = m_RespawnTime;
                }
            }
        }
    }

    void SetItemBox(bool value)
    {
        ItemBoxGFX.SetActive(value);
        ItemBoxCollider.enabled = value;
    }
}
