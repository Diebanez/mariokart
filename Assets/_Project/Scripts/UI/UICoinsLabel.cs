﻿using MarioKart.Movement;
using MLFramework.Core;
using TMPro;
using UnityEngine;

public class UICoinsLabel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_CoinsText;
    
    private KartMover m_TargetPlayer;

    private void OnEnable()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_TargetPlayer = GameObject.FindWithTag("Player").GetComponentInChildren<KartMover>();
    }

    private void Update()
    {
        if (GameManager.ActualState == GameStates.Gameplay)
            m_CoinsText.text = m_TargetPlayer.CollectedCoins.ToString();
    }
}