﻿using MarioKart.Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BananaPowerUp : PowerUp
{
    public GameObject BananaObject;
    public float HeightForce;
    public float LengthForce;
	public float GravityForce;

    public override void UsePowerUp(GameObject go, PowerUpHandler.UseDirection direction)
    {
        PowerUpHandler handler = go.transform.parent.GetComponentInChildren<PowerUpHandler>();

        if (direction == PowerUpHandler.UseDirection.Behind)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(BananaObject, handler.BackSpawner.transform.position, Quaternion.identity);
        }
        else if(direction == PowerUpHandler.UseDirection.Forward)
        {
            base.UsePowerUp(go, direction);
            GameObject obj = Instantiate(BananaObject, handler.FrontSpawner.transform.position, Quaternion.identity);
            obj.GetComponent<Rigidbody>().AddForce(handler.FrontSpawner.transform.forward * LengthForce + handler.FrontSpawner.transform.up * HeightForce, ForceMode.Impulse);
			obj.GetComponent<Rigidbody>().AddForce(-handler.FrontSpawner.transform.up * GravityForce, ForceMode.Force);
        }
    }
}
